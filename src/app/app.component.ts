import {Component, OnInit} from '@angular/core';
import {Team} from './model/Team';
import {HttpErrorResponse} from '@angular/common/http';
import {TeamService} from './services/team.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public teams: Team[] = [];
  // @ts-ignore
  public updateTeam: Team;
  // @ts-ignore
  public deleteTeam: Team;

  constructor(private teamService: TeamService) {
  }

  ngOnInit(): void {
    this.getTeams();
  }

  public getTeams(): void {
    this.teamService.getTeams().subscribe(
      (response: Team[]) => {
        this.teams = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onOpenModal(team: any, mode: string): void {
    const container = document.getElementById('main-container');
    let button: HTMLButtonElement;
    button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'add') {
      button.setAttribute('data-target', '#addTeamModal');
    }
    if (mode === 'edit') {
      this.updateTeam = team;
      button.setAttribute('data-target', '#editTeamModal');
    }
    if (mode === 'delete') {
      this.deleteTeam = team;
      button.setAttribute('data-target', '#deleteTeamModal');
    }
    // @ts-ignore
    container.appendChild(button);
    button.click();
  }

  onAddTeam(addForm: NgForm): void {
    // @ts-ignore
    document.getElementById('add-team-form').click();
    this.teamService.addTeam(addForm.value).subscribe(
      (response: Team) => {
        console.log(response);
        this.getTeams();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    );
  }

  onUpdateTeam(team: Team): void {
    this.teamService.updateTeam(team).subscribe(
      (response: Team) => {
        console.log(response);
        this.getTeams();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  onDeleteTeam(teamId: any): void {
    this.teamService.deleteTeam(teamId).subscribe(
      (response: void) => {
        console.log(response);
        this.getTeams();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public searchTeams(key: string): void {
    const result: Team[] = [];
    for (const team of this.teams) {
      if (team.name.toLowerCase().indexOf(key.toLowerCase()) !== -1
        || team.country.toLowerCase().indexOf(key.toLowerCase()) !== -1
        || team.city.toLowerCase().indexOf(key.toLowerCase()) !== -1
        || team.stadium.toLowerCase().indexOf(key.toLowerCase()) !== -1) {
        result.push(team);
      }
    }
    this.teams = result;
    if (result.length === 0 || !key) {
      this.getTeams();
    }
  }
}
