export interface Team {
  id: number;
  name: string;
  nickName: string;
  email: string;
  stadium: string;
  logo: string;
  webPage: string;
  city: string;
  country: string;
  teamCode: string;
}
